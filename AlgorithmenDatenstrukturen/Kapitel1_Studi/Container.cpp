#include "Container.h"

template<class T>
Container<T>::Container()
{
	itemcount = 0;
}

template<class T>
Container<T>::~Container()
{
}

template<class T>
int Container<T>::indexFirstEmpty()
{
	return 0;
}

template<class T>
T Container<T>::find(T item, std::function<bool(T, T)> equal)
{
	for (int i = 0; i < itemcount; i++)
	{
		if (equal(items[i], item))
		{
			return items[i];
		}
	}
	return nullptr;
}

template<class T>
bool Container<T>::append(T item)
{
	if (itemcount < 1000)
	{
		items[itemcount] = item;
		itemcount++;
		return true;
	}
	else return false;
}

template<class T>
bool Container<T>::remove(T key, std::function<bool(T, T)> equal)
{
	bool found = false;
	for (int i = 0; i < itemcount; i++)
	{
		if (!found)
		{
			if (equal(items[i], key))
			{
				items[i] = nullptr;
				found = true;
			}
		}
		else
		{
			if (i > 0)
			{
				items[i - 1] = items[i];
			}
			if (i >= itemcount - 1)
			{
				items[i] = nullptr;
				itemcount--;
			}
		}
	}
	return found;
}