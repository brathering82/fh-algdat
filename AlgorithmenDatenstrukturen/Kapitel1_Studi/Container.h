#pragma once
#include <functional>

template<class T>
class Container
{
public:
	Container();
	~Container();

	bool append(T);
	bool remove(T, std::function<bool(T,T)>);
	T find(T, std::function<bool (T,T)>);

private:
	int itemcount;
	T items[1000];
	int indexFirstEmpty(void);
};

