#include "Studi.h"
#include "Container.cpp"
#include <string>
#include <iostream>


Studi::Studi(char* vname, char* name, std::string matrikel, int gebTag, int gebMonat, int gebJahr)
{
	Name = name;
	Vname = vname;
	GebTag = gebTag;
	GebMonat = gebMonat;
	GebJahr = gebJahr;
	Matrikel = matrikel;
}


Studi::~Studi()
{
}

void Studi::Ausgeben()
{
	std::cout << "Daten von " << Vname << " " << Name << std::endl;
	std::cout << "Matrikelnummer: " << Matrikel << std::endl;
	std::cout << "Geburtsdatum: " << GebTag << "." << GebMonat << "." << GebJahr << std::endl;
}

bool Studi::kleiner(Studi* s1, Studi* s2)
{
	int m1, m2;
	m1 = std::stoi(s1->Matrikel);
	m2 = std::stoi(s2->Matrikel);

	if (m1 < m2) return true;
	else return false;
}

bool Studi::gleich(Studi* s1, Studi* s2)
{
	int m1, m2;
	m1 = std::stoi(s1->Matrikel);
	m2 = std::stoi(s2->Matrikel);

	if (m1 == m2) return true;
	else return false;
}