#include <iostream>
using namespace std;
#include <string>
#include "Studi.h"
#include "Container.cpp"

int main(){
	Container <Studi*>v;
	Studi* maske = new Studi("Hans", "Mueller", "12340007", 24, 3, 1988);
	Studi* maske2 = new Studi("Hans", "Mueller", "12340020", 24, 3, 1988);
	Studi* maske3 = new Studi("Hans", "Mueller", "12340044", 24, 3, 1988);
	Studi* s;

	for (int n = 0; n <= 20; n++)
	{
		int m = 12340000;

		v.append(new Studi("Hans", "Mueller", to_string(m+n), 24, 3, 1988));
	}
	
	v.remove(maske, Studi::gleich);

	s = v.find(maske2, Studi::gleich);
	if (s != nullptr)
	{
		cout << endl << "Student gefunden!" << endl << endl << endl;
		s->Ausgeben();
	}
	else cout << "Student nicht gefunden" << endl;

	v.append(new Studi("Hans", "Werner", "12340044", 24, 3, 1944));
	s = v.find(maske3, Studi::gleich);
	if (s != nullptr)
	{
		cout << endl << "Student gefunden!" << endl << endl << endl;
		s->Ausgeben();
	}
	else cout << "Student nicht gefunden" << endl;
	
	cin.get();
	return 0;
}