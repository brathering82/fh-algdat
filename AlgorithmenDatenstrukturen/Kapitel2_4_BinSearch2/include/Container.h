#pragma once
#include <functional>

template<class T>
class Container
{
public:
	Container();
	~Container();

	bool append(T);
	bool add(T, std::function<bool(T, T)>);
	bool insert(T, int);
	bool remove(T, std::function<bool(T,T)>);

	T find(T, std::function<bool (T,T)>);
	T getItem(int);
	bool isEmpty(void);
	bool isFull(void);
	int size(void);

	void print(std::function<void(T*)>);
private:
	int itemcount;
	int iter;
	const int MAX_SIZE = 1000;
	T items[1000];
	int indexFirstEmpty(void);
};

