#include "Stack.h"

template<class T>
Stack<T>::Stack()
{
	stackptr = -1;
}

template<class T>
Stack<T>::~Stack()
{
}

template<class T>
bool Stack<T>::isEmpty()
{
	if (stackptr >= 0) return false;
	else return true;
}

template<class T>
bool Stack<T>::isFull()
{
	if (stackptr < MAX_SIZE-1) return false;
	else return true;
}

template<class T>
int Stack<T>::size()
{
	if (isEmpty()) return 0;
	else return stackptr+1;
}

template<class T>
bool Stack<T>::push(T item)
{
	if (! isFull())
	{
		stackptr++;
		items[stackptr] = item;
		cout << "Item added to stack." << endl;
		return true;
	}
	else
	{
		cout << "Item skipped, stack is full!" << endl;
		return false;
	}
}

template<class T>
T Stack<T>::top(void)
{
	if (isEmpty()) throw new exception("Warning, return from empty stack!");// cout << "Warning, return from empty stack!" << endl;

	return items[stackptr];
}

template<class T>
bool Stack<T>::pop(void)
{
	if (!isEmpty())
	{
		stackptr--;
		cout << "Stack popped" << endl;
		return true;
	}
	else
	{
		cout << "Tried to pop an empty stack!" << endl;
		return false;
	}
}

template<class T>
void Stack<T>::print(void)
{
	cout << endl << "Stack contents" << endl;
	for (int i = 0; i <= stackptr; i++)
	{
		cout << i << ": " << items[i] << endl;
	}
	cout << "Itemcount: " << size() << endl << endl;
}