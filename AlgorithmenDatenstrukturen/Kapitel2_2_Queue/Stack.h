#pragma once

template<class T>
class Stack
{
public:
	bool isEmpty(void);
	bool isFull(void);
	bool push(T);
	bool pop(void);
	T top(void);
	int size(void);
	void print(void);

	Stack();
	~Stack();
private:
	int stackptr;
	const int MAX_SIZE = 5;
	T items[5];
};

