#pragma once
#include <string>

class Studi
{
public:
	Studi(char*, char*, std::string, int, int, int);
	Studi(std::string, std::string, std::string);
	Studi(void);
	~Studi();

	std::string namen[5];
	std::string vnamen[5];

	std::string Name;
	std::string Vname;
	unsigned int GebTag,GebMonat,GebJahr;
	std::string Matrikel;

	void Ausgeben(void);
	static bool kleiner(Studi*, Studi*);
	static bool gleich(Studi*, Studi*);
};

