#pragma once

template<class T>
class Ring
{
public:
	bool isEmpty(void);
	bool isFull(void);
	bool enQueue(T*);
	bool deQueue(void);
	int size(void);
	int getMaxSize(void);
	void print(void);
	void brief(void);

	Ring();
private:
	int Start;
	int End;
	int Count;
	const int MAX_SIZE=6;
	T* ring[6];

	int Prev(int);
	int Next(int);
};

