#include <iostream>
#include <string>
#include "Studi.h"
#include "Ring.cpp"
#include <stdlib.h>
#include <time.h>
using namespace std;

int main(){
	char command = 'a';
	int n = 0;
	Ring <Studi>buffer;

	srand(time(NULL));

	while (command != 'e')
	{
		cout << "Commands enQueueRandom(n), enQueue(q), deQueue(d), info(i), exit(e)" << endl;
		cin >> command;
		if (command == 'n')
		{
			Studi* s = new Studi();
			if (buffer.enQueue(s)) cout << "Studi added" << endl;
			else cout << "Fehler" << endl;
		}
		if (command == 'q')
		{
			//wohnheim.enQueue(verz[n]);
		}
		if (command == 'd')
		{
			if (buffer.deQueue()) cout << "Studi removed" << endl;
			else cout << "Fehler" << endl;
		}
		if (command == 'i')
		{
			buffer.print();
		}
	}

	cin.get();
	return 0;
}