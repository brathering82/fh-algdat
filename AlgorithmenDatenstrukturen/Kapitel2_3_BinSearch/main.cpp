#include <iostream>
#include <string>
#include <Studi.h>
#include <Container.cpp>
#include <stdlib.h>
#include <time.h>
#include <ctime>
using namespace std;

int main(){
	Container <Studi*>verz;
	char command = 'a';
	int n = 0;
	Studi* s = new Studi();

	srand(time(NULL));

	while (command != 'e')
	{
		cout << "Commands insertNewRandom(n), addBatchRandom(b), compareLinBinSearch(c), info(i), exit(e)" << endl;
		cin >> command;
		if (command == 'n')
		{
			verz.add(new Studi(), Studi::kleiner);
		}
		if (command == 'b')
		{
			int n;
			cout << "Amount of Students? (max " << verz.getMaxSize() << ")" << endl;
			cin >> n;

			for (int i = 0; i < n; i++)
			{
				verz.add(new Studi(), Studi::kleiner);
			}
		}
		if (command == 'c')
		{
			int pos;
			cout << "Binary search" << endl;
			cout << "Enter index of student between 0 and " << verz.size()-1 << " ";
			cin >> pos;

			clock_t ta, te;
			Studi* stud = verz.getItem(pos);
			int index=0;

			// binary
			ta = clock();
			index = verz.binSearch(stud, Studi::kleiner);
			te = clock();
			cout << "- Found at " << index << endl;
			cout << "- Searchtime binary: " << (double)(te - ta) << endl << endl;
			
			// linear
			ta = clock();
			index = verz.linSearch(stud, Studi::gleich);
			te = clock();
			cout << "- Found at " << index << endl;
			cout << "- Searchtime linear: " << (double)(te - ta) << endl << endl;

		}
		if (command == 'i')
		{
			Studi* stud;
			cout << "Students waiting: " << verz.size() << endl;
			for (int i = 0; i < verz.size(); i++)
			{
				cout << verz.getItem(i)->Matrikel << endl;
			}
		}
	}

	cin.get();
	return 0;
}