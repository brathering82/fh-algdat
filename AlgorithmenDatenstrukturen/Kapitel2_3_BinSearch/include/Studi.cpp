#include <Studi.h>
#include <Container.cpp>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <time.h>


Studi::Studi(char* vname, char* name, std::string matrikel, int gebTag, int gebMonat, int gebJahr)
{
	Name = name;
	Vname = vname;
	GebTag = gebTag;
	GebMonat = gebMonat;
	GebJahr = gebJahr;
	Matrikel = matrikel;
}

Studi::Studi(std::string vname, std::string name, std::string matrikel)
{
	Name = name;
	Vname = vname;
	GebTag = 1;
	GebMonat = 1;
	GebJahr = 1900;
	Matrikel = matrikel;
}

Studi::Studi(void)
{
	vnamen[0] = "Olga"; vnamen[1] = "Harry"; vnamen[2] = "Fester"; vnamen[3] = "Herrmann"; vnamen[4] = "Emmett";
	namen[0] = "Borg"; namen[1] = "Potter"; namen[2] = "Brown"; namen[3] = "Buck"; namen[4] = "Kurz";

	int a, b;
	a = rand() % 4;
	b = rand() % 4;

	Name = namen[a];
	Vname = vnamen[b];
	GebTag = rand() % 31;
	GebMonat = rand() % 13;
	GebJahr = 1900 + (rand() % 100);
	Matrikel = std::to_string(1000000 + (rand() % 9999999));
	//Matrikel = std::to_string(100 + (rand() % 900));
}

Studi::~Studi()
{
}

void Studi::Ausgeben()
{
	std::cout << std::endl << "Daten von " << Vname << " " << Name << " mit Matrikelnumer: " << Matrikel << std::endl;
	std::cout << "Geburtsdatum: " << GebTag << "." << GebMonat << "." << GebJahr << std::endl;
}

void Studi::Ausgeben(Studi* s)
{
	std::cout << std::endl << "Daten von " << s->Vname << " " << s->Name << " mit Matrikelnumer: " << s->Matrikel << std::endl;
	std::cout << "Geburtsdatum: " << s->GebTag << "." << s->GebMonat << "." << s->GebJahr << std::endl;
}

bool Studi::kleiner(Studi* s1, Studi* s2)
{
	int m1, m2;
	m1 = std::stoi(s1->Matrikel);
	m2 = std::stoi(s2->Matrikel);

	if (m1 < m2) return true;
	else return false;
}

bool Studi::gleich(Studi* s1, Studi* s2)
{
	int m1, m2;
	m1 = std::stoi(s1->Matrikel);
	m2 = std::stoi(s2->Matrikel);

	if (m1 == m2) return true;
	else return false;
}