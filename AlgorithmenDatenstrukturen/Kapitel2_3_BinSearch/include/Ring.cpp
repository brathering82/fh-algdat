#include "Ring.h"
#include <string>
#include <iostream>

template<class T>
Ring<T>::Ring()
{
	Start = 0;
	End = 0;
	Count = 0;

	for (int i = 0; i < MAX_SIZE; i++)
	{
		ring[i] = nullptr;
	}
}

template<class T>
int Ring<T>::Prev(int x)
{
	if (--x < 0) x = MAX_SIZE - 1;
	return x;
}

template<class T>
int Ring<T>::Next(int x)
{
	if (++x >= MAX_SIZE) x = 0;
	return x;
}

template<class T>
bool Ring<T>::isEmpty(void)
{
	if (Count == 0) return true;
	else return false;
}

template<class T>
bool Ring<T>::isFull(void)
{
	if (Count == MAX_SIZE) return true;
	else return false;
}

template<class T>
bool Ring<T>::enQueue(T* item)
{
	if (isFull()) return false;

	ring[End] = item;
	Count++;
	End = Next(End);

	brief();

	return true;
}

template<class T>
bool Ring<T>::deQueue(void)
{
	if (isEmpty()) return false;

	delete ring[Start];
	ring[Start] = nullptr;
	Count--;
	Start = Next(Start);

	brief();

	return true;
}

template<class T>
int Ring<T>::getMaxSize(void)
{
	return MAX_SIZE;
}

template<class T>
void Ring<T>::print(void)
{
	int i = Start;
	for (int j = 0; j < Count; j++)
	{
		std::cout << "Element" << (j + 1) << std::endl;
		ring[i]->Ausgeben();
		i = Next(i);
	}
	std::cout << "Items in queue: " << Count << endl;
}

template<class T>
void Ring<T>::brief(void)
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		if (ring[i] != nullptr)	cout << (char)219;
		else
		{
			cout << (char)176;
		}
	}
	cout << "Items in ringbuffer: " << Count << endl << endl;
}