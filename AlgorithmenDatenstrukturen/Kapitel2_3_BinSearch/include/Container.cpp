#include <Container.h>

template<class T>
Container<T>::Container()
{
	itemcount = 0;
}

template<class T>
Container<T>::~Container()
{
}

template<class T>
int Container<T>::indexFirstEmpty()
{
	return 0;
}

template<class T>
bool Container<T>::isEmpty()
{
	if (itemcount == 0) return true;
	else return false;
}

template<class T>
bool Container<T>::isFull()
{
	if (itemcount == MAX_SIZE) return true;
	else return false;
}

template<class T>
int Container<T>::size()
{
	return itemcount;
}

template<class T>
T Container<T>::find(T item, std::function<bool(T, T)> equal)
{
	for (int i = 0; i < itemcount; i++)
	{
		if (equal(items[i], item))
		{
			return items[i];
		}
	}
	return nullptr;
}

template<class T>
T Container<T>::getItem(int i)
{
	return items[i];
}

template<class T>
int Container<T>::binSearch(T item, std::function<bool(T, T)> smaller)
{
	int a = 0;
	int b = itemcount - 1;
	int index=0;
	while (a<=b)
	{
		index = (b - a)/2 + a;

		if (smaller(item,items[index]))
		{
			b = index - 1;
		}
		else if (smaller(items[index], item))
		{
			index++;
			a = index;
		}
		else return index;
	}
	return index;
}

template<class T>
int Container<T>::linSearch(T item, std::function<bool(T, T)> equal)
{
	int index = 0;
	while (!equal(items[index], item))
	{
		index++;
		if (index >= size()) return -1;
	}
	return index;
}

template<class T>
bool Container<T>::add(T item, std::function<bool(T, T)> smaller)
{
	int index;
	index = binSearch(item, smaller);
	if (index < 0) return false;
	insert(item, index);
	return true;
}

template<class T>
bool Container<T>::insert(T item, int pos)
{
	if (isFull()) return false;
	else if (pos == size())
	{
		append(item);
		return true;
	}
	else if (pos >= 0)
	{
		int iterator = size() - 1;

		while (iterator >= pos)
		{
			items[iterator + 1] = items[iterator];
			iterator--;
		}
		items[pos] = item;
		itemcount++;
		return true;
	}
	else return false;
}

template<class T>
bool Container<T>::append(T item)
{
	if (!isFull())
	{
		items[itemcount] = item;
		itemcount++;
		return true;
	}
	else return false;
}

template<class T>
bool Container<T>::remove(T key, std::function<bool(T, T)> equal)
{
	bool found = false;
	for (int i = 0; i < itemcount; i++)
	{
		if (!found)
		{
			if (equal(items[i], key))
			{
				items[i] = nullptr;
				found = true;
			}
		}
		else
		{
			if (i > 0)
			{
				items[i - 1] = items[i];
			}
			if (i >= itemcount - 1)
			{
				items[i] = nullptr;
				itemcount--;
			}
		}
	}
	return found;
}

template<class T>
void Container<T>::print(std::function<void(T*)> iprint)
{
	for (int i = 0; i < itemcount; i++)
	{
		iprint(items[i]);
	}
}

template<class T>
int Container<T>::getMaxSize()
{
	return MAX_SIZE;
}