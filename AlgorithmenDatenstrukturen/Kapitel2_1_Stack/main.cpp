#include <iostream>
using namespace std;
#include <string>
#include "Studi.h"
#include "Container.cpp"
#include "Stack.cpp"

int main(){
	Stack <float>stck;
	char command = 'a';

	while (command != 'e')
	{
		cout << "Commands add(a), top(t), pop(p), info(i), exit(e)" << endl;
		cin >> command;
		if (command == 'a')
		{
			float n;
			cout << "Add value: ";
			cin >> n;
			stck.push(n);
		}
		if (command == 't')
		{
			try {
			cout << "Top item: " << stck.top() << endl;
			}
			catch (exception* ex){
				cout << ex->what() << endl;
			}
		}
		if (command == 'p')
		{
			stck.pop();
		}
		if (command == 'i')
		{
			stck.print();
		}
	}

	/*Container <Studi*>v;
	Studi* maske = new Studi("Hans", "Mueller", "12340007", 24, 3, 1988);
	Studi* maske2 = new Studi("Hans", "Mueller", "12340020", 24, 3, 1988);
	Studi* maske3 = new Studi("Hans", "Mueller", "12340044", 24, 3, 1988);
	Studi* s;

	for (int n = 0; n <= 20; n++)
	{
		int m = 12340000;

		v.append(new Studi("Hans", "Mueller", to_string(m + n), 24, 3, 1988));
	}

	v.remove(maske, Studi::gleich);

	s = v.find(maske2, Studi::gleich);
	if (s != nullptr)
	{
		cout << endl << "Student gefunden!" << endl << endl << endl;
		s->Ausgeben();
	}
	else cout << "Student nicht gefunden" << endl;

	v.append(new Studi("Hans", "Werner", "12340044", 24, 3, 1944));
	s = v.find(maske3, Studi::gleich);
	if (s != nullptr)
	{
		cout << endl << "Student gefunden!" << endl << endl << endl;
		s->Ausgeben();
	}
	else cout << "Student nicht gefunden" << endl;
	*/

	cin.get();
	return 0;
}