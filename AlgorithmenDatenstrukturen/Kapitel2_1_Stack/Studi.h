#pragma once
#include <string>
class Studi
{
public:
	Studi(char*, char*, std::string, int, int, int);
	~Studi();

	char* Name;
	char* Vname;
	unsigned int GebTag,GebMonat,GebJahr;
	std::string Matrikel;

	void Ausgeben(void);
	static bool kleiner(Studi*, Studi*);
	static bool gleich(Studi*, Studi*);
};

